import React from 'react';

import './Form.css';

const Form = props => {
    return (
        <div className="Form">
            <input type="text" id="form-input-name" placeholder="Movie name" onChange={props.inputChange} value={props.value}/>
            <button className="form-add-button" onClick={props.addClick}>ADD</button>
        </div>
    );
};

export default Form;