import React, {Component} from 'react';
import Form from './components/Form/Form';
import Movie from './components/Movie/Movie';


import './App.css';

class App extends Component {

    state = {
        movies: [],
        input: ''
    };

    updateInputValue = event => {
        const input = event.target.value;
        this.setState({input});
    };

    addMovie = () => {
        const randomID = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

        const movies = [...this.state.movies];
        const movie = {name: this.state.input, id: randomID};

        if (this.state.input === '') {
            movie.name = 'Movie';
        }

        movies.push(movie);
        const input = '';
        this.setState({movies: movies, input: input});
    };

    removeMovie = event => {
        const movies = [...this.state.movies];
        const movie = movies.findIndex(x => ('remove-' + x.id) === event.target.id);

        movies.splice(movie, 1);
        this.setState({movies});
    };

    changeMovie = event => {
        const movies = [...this.state.movies];
        const movieIndex = movies.findIndex(x => (x.id) === event.target.id);
        const movie = {...movies[movieIndex]};

        movie.name = event.target.value;
        movies[movieIndex] = movie;

        this.setState({movies});
    };

    render() {
        return (
            <div className="App">
                <Form
                    inputChange={this.updateInputValue}
                    addClick={this.addMovie}
                    value={this.state.input}
                />
                <h3 className="watch-list-heading">To watch list:</h3>
                <div className="Movies">
                    {this.state.movies.map(movie =>
                        <Movie
                            name={movie.name}
                            removeClick={this.removeMovie}
                            id={movie.id}
                            key={movie.id}
                            changeMovie={this.changeMovie}
                        />
                    )}
                </div>
            </div>
        );
    }
}

export default App;
