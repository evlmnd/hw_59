import React, {Component} from 'react';

import './Movie.css';

class Movie extends Component {

    shouldComponentUpdate(nextProps) {
        console.log('+');
        return nextProps.name !== this.props.name;
    };

    render() {
        return (
            <div className="Movie">
                <input
                    type="text"
                    defaultValue={this.props.name}
                    onChange={this.props.changeMovie}
                    id={this.props.id}
                    className="Movie-input"
                />
                <button
                    onClick={this.props.removeClick}
                    id={'remove-' + this.props.id}
                    className="Movie-remove-button"
                >X</button>
            </div>
        );
    }
}

export default Movie;