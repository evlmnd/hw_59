import React from 'react';

import './Joke.css';

const Joke = (props) => {
    return (
        <div className="Joke">
            {props.joke}
        </div>
    );
};

export default Joke;