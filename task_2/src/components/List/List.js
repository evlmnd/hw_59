import React, {Component} from 'react';
import Joke from '../Joke/Joke';

import Chuck from '../../assets/images/chuck_norris.png';
import './List.css';

class List extends Component {
    render() {
        return(
            <div className="List">
                <img src={Chuck} alt="chuck" className="Chuck Chuck-1"/>
                <img src={Chuck} alt="chuck" className="Chuck Chuck-2"/>
                {this.props.jokes.map(joke => (
                    <Joke joke={joke.value} key={joke.id}/>
                ))}
            </div>
        )
    }
}

export default List;