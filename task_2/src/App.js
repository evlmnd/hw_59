import React, {Component} from 'react';
import List from './components/List/List';

import './App.css';

class App extends Component {

    state = {
        jokes: []
    };


    componentDidMount() {
        for (let i = 0; i < 9; i++) {
            fetch('https://api.chucknorris.io/jokes/random').then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Error');
            }).then(joke => {
                const newJoke = {value: joke.value, id: joke.id};
                const jokes = [...this.state.jokes];

                jokes.push(newJoke);

                this.setState({jokes});
            }).catch(error => {
                console.log(error);
            })
        }
    }

    render() {
        return (
            <div className="App">
                <List jokes={this.state.jokes}/>
            </div>
        );
    }
}

export default App;
